// C++ program to show the example of server application in
// socket programming
#include <cstring> 
#include <iostream> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h> 

using namespace std;

int main()
{
    int s_socket = socket(AF_INET, SOCK_STREAM, 0);

    sockaddr_in s_address;
    s_address.sin_family = AF_INET;
    s_address.sin_port = htons(8080);
    s_address.sin_addr.s_addr = INADDR_ANY;

    bind(s_socket, (struct sockaddr *) &s_address, sizeof(s_address));

    listen(s_socket, 5);

    while(true){
        int c_socket = accept(s_socket, nullptr, nullptr);
        char buffer [1024] = {0}; // 1KB buffer.
        recv(c_socket, buffer, sizeof(buffer), 0);
        cout << "[MSG-CLIENT] " << buffer << endl;
    }

    close(s_socket);
}