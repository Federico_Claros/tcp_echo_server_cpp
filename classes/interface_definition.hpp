#include <iomanip>
#include <bitset>

enum StatusCode { FAILED, OK };

struct Header
{
    unsigned short int message_size; // 2 Bytes size
    unsigned char message_type; // 1 Byte size
    unsigned char message_sequence;
};

struct LoginRequest
{
    struct Header header;
    char user_name [32]; // 32 Bytes size
    char password [32];
};

struct LoginResponse
{
    struct Header header;
    enum StatusCode status;
};

struct EchoRequest
{
    struct Header header;
    unsigned int message_size;
    char ** message;
};

struct EchoResponse
{
    struct Header header;
    unsigned int message_size;
    char ** message;
};