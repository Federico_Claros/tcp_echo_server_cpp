#include <iostream>
#include <string>

#include "simple_hash.hpp"

using namespace std;

int main()
{
    SimpleHash sh;
    string input;
    cout << "Please enter string, enter for finish. ";
    cin >> input;
    
    auto checksum = sh.checksum(input);
    bool are_equal = sh.is_data_valid(input, checksum);
    
    cout <<"Is checksum correct for " <<
        input << "? " << are_equal << endl;
}