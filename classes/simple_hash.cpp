#include <iostream>
#include <iomanip>

#include "simple_hash.hpp"

u_int8_t SimpleHash::checksum(std::string to_parse)
{
    //u_int8_t c;
    u_int8_t checksum, sum = 0;
    
    //std::cout << "char\t | u_int_8\t | complemento 2"<<std::endl;
    for(auto &ch : to_parse)
      {
        //c = ch;
        //c = ~c+1; //saca complemento a 2.
        sum += ch;
        /*
        std::cout << ch << "\t | " << 
            std::hex << (0xff & (unsigned int)ch) <<
            "\t\t | " << 
            std::hex << (0xff & (unsigned int)c) << std::endl;
        */
      }
    std::cout << "Sum: " << (0xff & (unsigned int)sum) << std::endl;
    checksum = ~sum;
    std::cout << "Checksum: " << (0xff & (unsigned int)checksum) << std::endl;
    return checksum;
}

bool SimpleHash::is_data_valid(std::string data, u_int8_t checksum)
{
  return ((this->checksum(data) ^ checksum) == 0 ) ? true : false;
}