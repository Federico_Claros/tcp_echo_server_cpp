#include <string>

#include "interface_definition.hpp"

class Serializer
{
    public:
        std::string serialize_header(struct Header header);
        std::string serialize_login_request(
            struct LoginRequest login_request);
};