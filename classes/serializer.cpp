#include <iostream>
#include "serializer.hpp"

std::string Serializer::serialize_header(struct Header header)
{
    std::string str_header = "";

    str_header = std::to_string(header.message_size);
    std::cout << "Header: " << str_header << std::endl;
    str_header += static_cast<char>(header.message_type);
    std::cout << "Header: " << str_header << std::endl;
    str_header += static_cast<char>(header.message_sequence);
    std::cout << "Header: " << str_header << std::endl;

    return str_header;
}

std::string Serializer::serialize_login_request
(
    struct LoginRequest login_request
)
{
    std::string str_login_request = "";

    str_login_request += this->serialize_header(
        login_request.header);
    str_login_request += login_request.user_name;
    str_login_request += login_request.password;

    return str_login_request;
}