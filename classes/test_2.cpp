#include <iostream>
#include <string>
#include <bits/stdc++.h> 

#include "serializer.hpp"

using namespace std;

int main()
{
    Serializer serializer;
    struct LoginRequest login_request;

    cout << "Please enter username, enter for finish. ";
    cin >> login_request.user_name;
    cout << "Please enter password, enter for finish. ";
    cin >> login_request.password;
    
    /* init. header (very important!!)*/
    memset(&(login_request.header.message_type),
        0, sizeof(login_request.header.message_type));
    memset(&(login_request.header.message_sequence),
        0, sizeof(login_request.header.message_sequence));
    memset(&(login_request.header.message_size),
        0, sizeof(login_request.header.message_size));
    

    login_request.header.message_type = 'R';
    login_request.header.message_sequence = '1';
    login_request.header.message_size = sizeof(login_request);

    cout << "Header size: " << sizeof(login_request.header) << endl;
    cout << "\tHeader type: " <<
        login_request.header.message_type << endl;
    cout << "\tHeader sequence: " << 
        login_request.header.message_sequence << endl;
    cout << "Login Request size: " << sizeof(login_request) << endl;
    cout << "\tUser: " << login_request.user_name << endl;
    cout << "\tPassword: " << login_request.password << endl;

    std::string serial = serializer.serialize_login_request(
                                    login_request);

    cout <<"Serialization: " << serial << endl;

    /* 
        We know that regardless of the type of request or Response
        The header is fixed in size: 4 Bytes.
        So a server socket, by default, must reserve a 4 Byte Buffer,
            in order to read the rest data.
        Plus, inside this header.size -> there's the rest of the data.
            By doing header.size - 4 = Rest of data in Bytes.
        So by doing a receive in multiple chunks of 1K = 1024 Bytes.
            We can have all the messages we want.
    */
}