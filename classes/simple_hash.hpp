#include <string>
#include <cstdint>

class SimpleHash
{
    public:
        u_int8_t checksum(std::string to_parse);
        bool is_data_valid(std::string, u_int8_t checksum);
};