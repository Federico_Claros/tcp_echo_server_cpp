
// C++ program to illustrate the client application in the 
// socket programming 
#include <cstring> 
#include <iostream> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h> 
  
int main() 
{  
    int c_socket = socket(AF_INET, SOCK_STREAM, 0); 

    sockaddr_in serverAddress; 
    serverAddress.sin_family = AF_INET; 
    serverAddress.sin_port = htons(8080); 
    serverAddress.sin_addr.s_addr = INADDR_ANY; 

    connect(c_socket, (struct sockaddr*)&serverAddress, sizeof(serverAddress));

    // sending data
    const char* message = "Hello, server!";
    send(c_socket, message, strlen(message), 0);

    // closing socket 
    close(c_socket);
    return 0; 
}
